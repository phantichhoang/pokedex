import Enzyme, { shallow } from 'enzyme';
import Pokemon from "./Pokemon";
import Adapter from '@mahpooya/enzyme-adapter-react-17';

/**
 * Test call api ok
 */
it('should render a thumbnail container with a linear gradient background based on the pokemon type', () => {
// Given
const id = 1;
const image = "https://example.com/pokemon.png";
const name = "Bulbasaur";
const type = [{ type: { name: "grass" } }, { type: { name: "poison" } }];
const onElemClick = jest.fn();

Enzyme.configure({ adapter: new Adapter() });

// When
const wrapper = shallow(<Pokemon id={id} image={image} name={name} type={type} onElemClick={onElemClick} />);

// Then
expect(wrapper.find('.thumbnail__container')
    .prop('style'))
    .toHaveProperty('background', 'linear-gradient(#a8ff98, #d6a2e4)');
});
