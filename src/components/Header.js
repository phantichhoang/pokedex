import React from 'react';
import GitHubIcon from '@material-ui/icons/GitHub';
import Pokedex from "../assets/images/pokedex.png";
import {Image} from "@material-ui/icons";

class Header extends React.Component {

    changeTheme = () => {

        // debugger
        const currentTheme = document.documentElement.getAttribute('data-theme');
        // console.log(currentTheme);

        let targetTheme = "light";

        if (currentTheme === "light") {
            targetTheme = "dark";

            this.setState({
                isChecked: true,
            })

            // console.log(targetTheme);
        } else {
            this.setState({
                isChecked: false,
            })
        }
        document.documentElement.setAttribute('data-theme', targetTheme)
    }

    openGithub = () => {
        window.open("https://gitlab.com/phantichhoang/pokedex");
    }

    render() {
        return (
            <>
                <div className="app__header">
                    <div className="switch">

                        <div className="toggle">
                            <label htmlFor="themeSwitch"></label>
                            <input type="checkbox" name="swich-theme" id="themeSwitch" onClick={this.changeTheme} defaultChecked />
                            <div className="toggle-bg"></div>
                            <div className="toggle-thumb">
                                <i className="fas fa-sun"></i>
                                <i className="fas fa-moon"></i>
                            </div>
                        </div>
                    </div>
                    <div className="poke__logos noselect">
                        <img src={Pokedex} alt="pokelogo" className="poke__logo" />
                    </div>
                    <div className="pokeball__box github__icon" onClick={this.openGithub}>
                        <Image width={25} height={25} src='https://www.google.com/url?sa=i&url=https%3A%2F%2Fplantscience.psu.edu%2Fresearch%2Flabs%2Froots%2Fimages%2Fdeeper-site%2Fgitlab-logo%2Fimage_view_fullscreen&psig=AOvVaw1O7t8d5gczSF4kzMHq0o0q&ust=1705405534054000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCJjsne2o34MDFQAAAAAdAAAAABAD' />
                        {/*<GitHubIcon></GitHubIcon>*/}
                    </div>
                </div>
            </>
        )
    }
}

export default Header;
